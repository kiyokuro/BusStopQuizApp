/*
パッケージ名に注意！
ここは自分のプロジェクト名によって違う
*/
package com.application.toshi.busstopquizapp;


/*
インポート文
*/
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


/*
アプリが起動するとまずMainActivityが表示される
Activityは一つの画面のこと！
*/
public class MainActivity extends AppCompatActivity {


    /*
    MainActivityなどの画面が呼び出された時にまず実行されるのが
    下のonCreateっていうメソッド！
    実行したい処理はここに書くよ！
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        レイアウトの指定
         */
        setContentView(R.layout.activity_main);
    }

    /*
    レイアウトのonClickで指定した名前で、メソッドを定義！
    これでボタンが押されるとこの中の処理が実行される
     */
    public void startQuiz(View view){
        /*
        Intentを使って別のActivityを起動しているよ！
        今回はQuizActivity
         */
        Intent intent = new Intent(this,QuizActivity.class);
        startActivity(intent);
    }


    /*
    これより下は今回は気にしなくていいよ
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
